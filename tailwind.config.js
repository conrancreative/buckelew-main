module.exports = {
  content: ["./index.html","./success.html"],
  theme: {
    extend: {
      colors: {
        'fireworks-red':"#BF0F0F",
        'fireworks-blue-light': "#1c1a53",
        'fireworks-blue-dark': "#0b1035",
        'terror-yellow': "#FC0",
        'christmas-green': "#028a53",
        'buckelew-primary': "#51630f",
        'buckelew-secondary': "#dfb16e",
        'buckelew-tertiary': "#4d90e9",
        'buckelew-dark': "#313b0b",
        'social-fb': "#4267B2",
        'social-youtube': "#FF0000",
        'social-instagram': "#C13584",
        'social-twitter': "#1DA1F2",
        'social-tiktok': "#00f2ea",

      },
      fontFamily: {
        'main': ['new-spirit-condensed','serif'],
        'body' : ['Roboto', 'sans-serif'],
      },
      backgroundImage: {
        'fireworks': "url('/images/hero-bg.jpg')",
        'intro': "url('/images/fireworks-vector-bg.jpg')",
        'temp-map': "url('/images/temp-map.jpg')",
        'flag': "url('/images/flag-bg.jpg')",
        'hero-simple': "url('/images/hero-simple.jpg')",
        'buckelew-farm-bg': "url('/images/buckelew-farm-bg.jpg')",
        'terror': "url('/images/terror-bg.jpeg')",
        'christmas': "url('/images/christmas.jpg')",
      }
    },
  },
  plugins: [],
}
